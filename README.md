# AWS S3 Stream-Wrapper

Provides a Drupal stream-wrapper service backed by S3.

This module uses the AWS PHP SDK version 3.

## Credentials

Credentials must be provided the AWS default credential provider chain.

See [Credentials for the AWS SDK for PHP Version 3](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html)

One exampe approach is to use a standard AWS configuration file, and store it
in a location accessible to the server, but _outside_ the docroot.

For example, this code in `settings.php` would supply the location:

    putenv('AWS_SHARED_CREDENTIALS_FILE=/path/to/my/.aws-credentials');

## Simple usage (programmatic)

The `s3://` stream wrapper is defined and available programatically (but not
via the Drupal UI).

You may access S3 data using URIs following this pattern:

    s3://<bucketname>/path/to/file.txt

## Stream wrapper aliases

You can create stream wrapper aliases, such as `image://` or `whitepaper://`
via services, or through the admin UI.

This is generally used in conjunction with predefined buckets and optional
bucket path prefixes.

For example, your `image` alias may use the bucket `acme-corp-assets` with a
path prefix of `images/website`.

This would allow you to use `image://AcmeLogo.png` which would resolve to the
S3 destination `s3://acme-corp-assets/images/website/AcmeLogo.png`.

This is useful on multisite implementations; for example, a UK website may
configure the path prefix `images/website/uk` whilst the USA website may
configure the path prefix `images/website/us`. Both sites could use the
`image://` wrapper convention in code (and in field aliases), whilst
organizing the storage in a structured way.

### Aliases via a service definition

    # Stream wrapper for the 'image://' protocol, using the bucket name
    # acme-corp-assets.
    # The stream wrapper is configured to be visible in the Drupal UI.
    mymodule.stream_wrapper.image:
      parent: aws_s3_stream_wrapper.stream_wrapper
      tags:
        -
          name: s3_stream_wrapper
          scheme: image
          bucket_name: acme-corp-assets
          bucket_path_prefix: 'images/website/uk'
          visible: true

### Aliases via configuration

Aliases can be added and managed using the admin configuration form in the
section: `/admin/config/media/file-system/s3-stream-wrappers`.

Each alias creates a configuration entry with the prefix:
`aws_s3_stream_wrapper.stream_wrapper.` followed by the scheme.

## Footnotes

### Bucketname uniqueness

From [AWS Buckets overview](https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingBucket.html):

> "An Amazon S3 bucket name is globally unique, and the namespace is shared by
   all AWS accounts. This means that after a bucket is created, the name of
   that bucket cannot be used by another AWS account in any AWS Region until
   the bucket is deleted."

### Permission quirks

A `mkdir` operation on a _private_ bucket may return Access Denied, because the
default permissions are public. Instead of `mkdir('s3://bucket/foo');`, use
`mkdir('s3://bucket/foo', 0400);`.
