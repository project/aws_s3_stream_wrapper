<?php

namespace Drupal\aws_s3_stream_wrapper\Form;

use Drupal\Core\DrupalKernel;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Confirmation form displayed when deleteing a dynamic stream wrapper.
 */
class DeleteWrapperForm extends ConfirmFormBase {

  /**
   * Scheme name of the defined stream wrapper scheme to delete.
   *
   * @var string
   */
  protected $scheme;

  /**
   * Drupal kernel service.
   *
   * @var \Drupal\Core\DrupalKernel
   */
  protected $kernel;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   Drupal kernel.
   */
  public function __construct(DrupalKernel $kernel) {
    $this->kernel = $kernel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('kernel')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $scheme = NULL) {
    $this->scheme = $scheme;
    $configKey = EditWrapperConfigurationForm::CONFIG_KEY_PREFIX . '.' . $this->scheme;
    $config = $this->config($configKey);
    if (empty($config->get())) {
      throw new NotFoundHttpException();
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, $scheme = NULL) {
    $config = $this->configFactory->getEditable(EditWrapperConfigurationForm::CONFIG_KEY_PREFIX . '.' . $this->scheme);
    $config->delete();

    $this->messenger()->addStatus($this->t('The stream wrapper for scheme %id has been deleted.', ['%id' => $this->scheme]));

    // Mark the container for rebuild, to apply the changes in a new compiler
    // pass.
    $this->kernel->invalidateContainer();

    $form_state->setRedirect('aws_s3_stream_wrapper.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_s3_stream_wrapper.delete_stream_wrapper_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('aws_s3_stream_wrapper.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete the stream wrapper for scheme %id?', ['%id' => $this->scheme]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will not delete any files from the stream wrapper, but Drupal will not be able to access those files any more.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete stream wrapper');
  }

}
