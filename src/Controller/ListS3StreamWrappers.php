<?php

namespace Drupal\aws_s3_stream_wrapper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * List the dynamic S3 wrappers.
 */
class ListS3StreamWrappers extends ControllerBase {

  /**
   * Stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   Stream wrapper manager service.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    $tableHeaders = [
      'scheme'             => $this->t('Scheme'),
      'name'               => $this->t('Name'),
      'bucket_name'        => $this->t('Bucket'),
      'bucket_path_prefix' => $this->t('Path prefix'),
      'visibility'         => $this->t('Visibility'),
      'operations'         => $this->t('Operations'),
    ];

    $build = [
      '#attached' => [
        'library' => [
          'aws_s3_stream_wrapper/admin',
        ],
      ],
    ];

    $build['stream_wrappers'] = [
      '#attributes' => [
        'class' => [
          'aws_s3_stream_wrappers_list',
        ],
      ],
      '#type' => 'table',
      '#header' => $tableHeaders,
      '#empty' => $this->t('No defined S3 stream wrappers.'),
    ];

    $s3Wrappers = $this->streamWrapperManager->getS3WrapperInfo();

    // Sort the wrappers so that the default protocol `s3://` is always top.
    uksort($s3Wrappers, function ($a, $b) {
      if ($a === 's3' || $b === 's3') {
        return ($a === 's3') ? -1 : 1;
      }
      return strnatcmp($a, $b);
    });

    foreach ($s3Wrappers as $scheme => $info) {
      $wrapper = $this->streamWrapperManager->getViaScheme($scheme);

      $row = [
        '#attributes' => [
          'class' => 'service-' . $info['registration'],
        ],
        'scheme' => [
          '#plain_text' => $info['scheme'],
        ],
        'name' => [
          '#plain_text' => $wrapper->getName(),
        ],
        'bucket_name' => [
          '#plain_text' => $info['bucket_name'] ?? '-',
        ],
        'bucket_path_prefix' => [
          '#plain_text' => $info['bucket_path_prefix'] ?? '-',
        ],
        'visibility' => [
          '#plain_text' => ($wrapper::getType() & StreamWrapperInterface::VISIBLE) ? $this->t('Visible') : $this->t('Hidden'),
        ],
        'operations' => [],
      ];

      if ($info['registration'] === $this->streamWrapperManager::SERVICE_DYNAMIC) {
        $row['operations']['data'] = [
          '#type' => 'operations',
          '#links' => $this->operations($wrapper),
        ];
      }
      else {
        $row['operations'][] = [
          '#markup' => $this->t('See service: %service_id', ['%service_id' => $info['service_id']]),
        ];
      }

      $build['stream_wrappers'][] = $row;
    }

    return $build;
  }

  /**
   * Fetch a list of operations available to a stream wrapper.
   *
   * @return array
   *   Array of links to use in an operations render element.
   */
  protected function operations(StreamWrapperInterface $wrapper) {
    $ops = [];
    $ops['edit'] = [
      'title' => $this->t('Edit configuration'),
      'url' => Url::fromRoute('aws_s3_stream_wrapper.edit_configuration', ['scheme' => $wrapper->getProtocol()]),
    ];
    $ops['delete'] = [
      'title' => $this->t('Delete'),
      'url' => Url::fromRoute('aws_s3_stream_wrapper.delete_configuration', ['scheme' => $wrapper->getProtocol()]),
    ];
    return $ops;
  }

}
