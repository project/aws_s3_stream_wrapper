<?php

namespace Drupal\aws_s3_stream_wrapper;

use Aws\Sdk;

/**
 * Factory to create an S3 multi-region client.
 */
class S3ClientFactory {

  /**
   * Version of the AWS API. This is (currently) the latest.
   *
   * @param string
   */
  const VERSION = '2006-03-01';

  /**
   * Create an S3 client.
   */
  public static function createS3Client($args = []) {
    // Enforce the API version.
    $args['version'] = self::VERSION;

    // @todo Add the http client handler.
    // @todo Create a credentials loader.
    $client = (new Sdk)->createMultiRegionS3($args);
    return $client;
  }

}
