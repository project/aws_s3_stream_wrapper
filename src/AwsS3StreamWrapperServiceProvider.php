<?php

namespace Drupal\aws_s3_stream_wrapper;

use Drupal\aws_s3_stream_wrapper\Compiler\RegisterDynamicStreamWrapperServices;
use Drupal\aws_s3_stream_wrapper\Compiler\RegisterS3StreamWrappersPass;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;

/**
 * Adds a compiler pass to register dynamic S3 stream wrappers.
 */
class AwsS3StreamWrapperServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // This compiler pass identifies all the dynamic S3 stream wrappers (which
    // are identified in config) and adds them as services.
    $container->addCompilerPass(new RegisterDynamicStreamWrapperServices(), PassConfig::TYPE_OPTIMIZE, -5);

    // This compiler pass adds the defined services to the stream wrapper
    // manager. Note that this must run AFTER the container service decorator
    // pass, so that the `stream_wrapper_manager` service is the decorated
    // service.
    // @see \Symfony\Component\DependencyInjection\Compiler\DecoratorServicePass
    $container->addCompilerPass(new RegisterS3StreamWrappersPass(), PassConfig::TYPE_OPTIMIZE, -10);
  }

}
