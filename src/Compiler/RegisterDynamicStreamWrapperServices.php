<?php

namespace Drupal\aws_s3_stream_wrapper\Compiler;

use Drupal\aws_s3_stream_wrapper\StreamWrapper\S3StreamWrapperManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Register each dynamically-generated S3 stream wrapper as a service.
 */
class RegisterDynamicStreamWrapperServices implements CompilerPassInterface {

  /**
   * Class which implements the S3 stream wrapper service for Drupal.
   *
   * @var string
   */
  const S3_STREAM_WRAPPER_CLASS = 'Drupal\aws_s3_stream_wrapper\StreamWrapper\S3StreamWrapper';

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition('config.factory') || !$container->hasDefinition('aws_s3_stream_wrapper.stream_wrapper')) {
      return;
    }
    $configFactory = $container->get('config.factory');
    $parentDefinition = $container->getDefinition('aws_s3_stream_wrapper.stream_wrapper');
    $parentDefinition->setAbstract(FALSE);

    $configKeys = $configFactory->listAll(S3StreamWrapperManager::CONFIG_KEY_PREFIX);
    foreach ($configFactory->loadMultiple($configKeys) as $key => $config) {
      // The service ID will match the config ID.
      // Skip services which already have a definition.
      if ($container->hasDefinition($key)) {
        continue;
      }

      // Each of the config properties are passed as service tags.
      $tag = $config->get();
      $tag['registration'] = S3StreamWrapperManager::SERVICE_DYNAMIC;

      // Add the dynamic stream-wrapper as a service.
      $container->setDefinition($key, clone($parentDefinition))
        ->addTag('s3_stream_wrapper', $tag);
    }
  }

}
