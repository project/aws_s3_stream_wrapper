<?php

namespace Drupal\aws_s3_stream_wrapper\Compiler;

use Drupal\aws_s3_stream_wrapper\StreamWrapper\S3StreamWrapperManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Attach S3 stream wrappers to the stream_wrapper_manager service.
 */
class RegisterS3StreamWrappersPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    // The container should have the `stream_wrapper_manager` service as an
    // alias to the decorator service.
    if (!$container->hasAlias('stream_wrapper_manager')) {
      return;
    }

    $decoratorService = (string) $container->getAlias('stream_wrapper_manager');
    if (!$container->hasDefinition($decoratorService)) {
      return;
    }

    $stream_wrapper_manager_interface = $container->getDefinition($decoratorService);

    foreach ($container->findTaggedServiceIds('s3_stream_wrapper') as $id => $attributes) {
      $stream_wrapper_definition = $container->getDefinition($id);

      $registration = $attributes[0]['registration'] ?? S3StreamWrapperManager::SERVICE_STATIC;
      $scheme       = $attributes[0]['scheme'];
      $bucket       = $attributes[0]['bucket_name'] ?? NULL;
      $prefix       = $attributes[0]['bucket_path_prefix'] ?? NULL;

      // If the wrapper is configured to be visible in the UI, change the class.
      if (!empty($attributes[0]['visible'])) {
        $newClass = $stream_wrapper_definition->getClass() . 'Visible';
        if (class_exists($newClass)) {
          $stream_wrapper_definition->setClass($newClass);
        }
      }

      $stream_wrapper_manager_interface->addMethodCall('addStreamWrapper', [
        $id,
        $stream_wrapper_definition->getClass(),
        $scheme,
      ]);
      $stream_wrapper_manager_interface->addMethodCall('registerS3Wrapper', [
        $scheme,
        $registration,
      ]);
      $stream_wrapper_manager_interface->addMethodCall('registerS3WrapperAliasing', [
        $scheme,
        $bucket,
        $prefix,
      ]);

      $stream_wrapper_definition->addMethodCall('setServiceId', [$id]);
      $stream_wrapper_definition->addMethodCall('setProtocol', [$scheme]);

      // Ensure the service ID is the first method call during instantiation.
      $ordered_method_calls = [
        // Placeholder for the setServiceId method call, as the first method.
        0 => [],
      ];
      foreach ($stream_wrapper_definition->getMethodCalls() as $method) {
        if ($method[0] === 'setServiceId') {
          $ordered_method_calls[0] = $method;
        }
        else {
          $ordered_method_calls[] = $method;
        }
      }
      $stream_wrapper_definition->setMethodCalls($ordered_method_calls);
    }
  }

}
