<?php

namespace Drupal\aws_s3_stream_wrapper\StreamWrapper;

use Aws\CacheInterface;
use Aws\LruArrayCache;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Stream wrapper manager for S3 stream-wrappers.
 */
class S3StreamWrapperManager implements StreamWrapperManagerInterface {

  /**
   * Config key prefix for dynamic stream wrappers defined in config.
   *
   * @var string
   */
  const CONFIG_KEY_PREFIX = 'aws_s3_stream_wrapper.stream_wrapper.';

  /**
   * Class which implements the S3 stream wrapper service for Drupal.
   *
   * @var string
   */
  const S3_STREAM_WRAPPER_CLASS = 'Drupal\aws_s3_stream_wrapper\StreamWrapper\S3StreamWrapper';

  /**
   * The S3 stream wrapper is registered statically through the service layer.
   *
   * @var string
   */
  const SERVICE_STATIC = 'static';

  /**
   * The S3 stream wrapper is registered at run-time as a dynamic service.
   *
   * @var string
   */
  const SERVICE_DYNAMIC = 'dynamic';

  /**
   * Decorated stream-wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  private $decorated;

  /**
   * The S3 client service.
   *
   * @var \Aws\S3\S3ClientInterface
   */
  protected $defaultS3Client;

  /**
   * Defined S3 wrappers and their config.
   *
   * @var array
   */
  protected $s3Wrappers = [];

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $decorated
   *   The stream wrapper manager service to be decorated.
   * @param \Aws\S3\S3ClientInterface $s3_client
   *   The AWS S3 client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(StreamWrapperManagerInterface $decorated, S3ClientInterface $s3_client, ConfigFactoryInterface $config_factory) {
    $this->decorated = $decorated;
    $this->defaultS3Client = $s3_client;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getWrappers($filter = StreamWrapperInterface::ALL) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getNames($filter = StreamWrapperInterface::ALL) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getDescriptions($filter = StreamWrapperInterface::ALL) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getViaScheme($scheme) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getViaUri($uri) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getClass($scheme) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function registerWrapper($scheme, $class, $type) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public static function getTarget($uri) {
    return self::__callStatic(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeUri($uri) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public static function getScheme($uri) {
    return self::__callStatic(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function isValidScheme($scheme) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function isValidUri($uri) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function __call($name, $arguments) {
    return $this->decorated->$name(...$arguments);
  }

  /**
   * {@inheritdoc}
   */
  public static function __callStatic($name, $arguments) {
    return StreamWrapperManager::$name(...$arguments);
  }

  /**
   * Register an S3 stream wrapper.
   *
   * @param string $scheme
   *   If the stream was "public://target", "public" would be the scheme.
   * @param string $registration
   *   Whether the service is registered in the service layer or at run time.
   * @param \Aws\S3\S3ClientInterface $client
   *   (optional) The AWS S3 client.
   * @param \Aws\CacheInterface $cache
   *   (optional) A cache service.
   */
  public function registerS3Wrapper($scheme, $registration = self::SERVICE_STATIC, S3ClientInterface $client = NULL, CacheInterface $cache = NULL) {
    if (!$client) {
      $client = $this->defaultS3Client;
    }
    if (!$cache) {
      $cache = new LruArrayCache();
    }

    $default = stream_context_get_options(stream_context_get_default());

    if (empty($default[$scheme]) || empty($default[$scheme]['client'])) {
      // Attach the 'client' property.
      $default[$scheme]['client'] = $client;

      if (empty($default[$scheme]['cache'])) {
        $default[$scheme]['cache'] = $cache;
      }
      stream_context_set_default($default);
    }

    $this->s3Wrappers[$scheme] = [
      'scheme'             => $scheme,
      'registration'       => $registration,
      'bucket_name'        => '',
      'bucket_path_prefix' => '',
      'service_id'         => '',
    ];
  }

  /**
   * Register aliasing against a bucket and optional path prefix.
   *
   * @param string $scheme
   *   The URI scheme for this wrapper.
   * @param string $bucket
   *   The name of an AWS S3 bucket.
   * @param string $bucket_path_prefix
   *   (optional) The path prefix to apply when using this wrapper with the
   *   selected bucket.
   */
  public function registerS3WrapperAliasing($scheme, $bucket, $bucket_path_prefix = NULL) {
    $default = stream_context_get_options(stream_context_get_default());
    $default[$scheme]['s3BucketName'] = $bucket;
    $default[$scheme]['s3BucketPathPrefix'] = $bucket_path_prefix;
    stream_context_set_default($default);

    $this->s3Wrappers[$scheme]['bucket_name'] = $bucket;
    $this->s3Wrappers[$scheme]['bucket_path_prefix'] = $bucket_path_prefix;
  }

  /**
   * Get the defined stream wrapper info backed by S3.
   *
   * @return array
   *   Array of wrapper info, keyed by scheme.
   */
  public function getS3WrapperInfo() {
    $wrappers = [];
    foreach ($this->s3Wrappers as $scheme => $info) {
      $info['service_id'] = $this->decorated->getViaScheme($scheme)->_serviceId;
      $wrappers[$scheme] = $info;
    }
    return $wrappers;
  }

}
