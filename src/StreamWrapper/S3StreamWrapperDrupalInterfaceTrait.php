<?php

namespace Drupal\aws_s3_stream_wrapper\StreamWrapper;

/**
 * Implementation of interface methods required by StreamWrapperInterface.
 */
trait S3StreamWrapperDrupalInterfaceTrait {

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return static::STREAM_WRAPPER_TYPE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->config['name'] ?? $this->getProtocol();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->config['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    // Not implemented by this stream-wrapper.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function realpath() {
    // Not implemented by this stream-wrapper.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function dirname($uri = NULL) {
    if ($pos = strrpos($uri, '/')) {
      return substr($uri, 0, $pos);
    }
    return FALSE;
  }

}
