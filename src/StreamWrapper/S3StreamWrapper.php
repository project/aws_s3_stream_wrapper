<?php

namespace Drupal\aws_s3_stream_wrapper\StreamWrapper;

use Aws\S3\StreamWrapper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;

/**
 * Provide a Drupal-based stream-wrapper against AWS S3 buckets.
 */
class S3StreamWrapper extends StreamWrapper implements StreamWrapperInterface {

  use S3StreamWrapperDrupalInterfaceTrait;
  use S3StreamWrapperPrefixedPathsTrait;

  /**
   * An S3 stream wrapper which is hidden from the Drupal UI.
   *
   * These are stream wrapper bit flags that are the basis for composite types.
   * Note that StreamWrapperInterface::HIDDEN is a composite of READ and WRITE.
   *
   * @var int
   */
  const STREAM_WRAPPER_TYPE = StreamWrapperInterface::READ + StreamWrapperInterface::WRITE;

  /**
   * Config definition.
   *
   * @var array
   */
  protected $config;

  /**
   * Service ID defined in the container.
   *
   * @var string
   */
  protected $serviceId;

  /**
   * The protocol defined for this service, such as 's3'.
   *
   * @var string
   */
  private $protocol;

  /**
   * Set the service ID for this stream-wrapper service.
   *
   * @param string $service_id
   *   The ID defined in the container.
   */
  public function setServiceId($service_id) {
    $this->serviceId = $service_id;
  }

  /**
   * Set the protocol for this stream-wrapper service.
   *
   * @param string $protocol
   *   The protocol defined for this service, such as 's3'.
   */
  public function setProtocol($protocol) {
    $this->protocol = $protocol;
  }

  /**
   * Get the protocol for this stream-wrapper service.
   *
   * @return string
   *   The protocol defined for this service, such as 's3'.
   */
  public function getProtocol() {
    return $this->protocol;
  }

  /**
   * Set the config.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function setConfig(ConfigFactoryInterface $config_factory) {
    if ($this->serviceId && $config = $config_factory->get($this->serviceId)) {
      $this->config = array_merge([
        'description' => 'Files hosted on AWS S3',
        'type' => self::READ & self::WRITE,
      ], $config->get());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function stream_lock($operation) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_metadata($path, $option, $value) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_set_option($option, $arg1, $arg2) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_truncate($new_size) {
    return FALSE;
  }

}
