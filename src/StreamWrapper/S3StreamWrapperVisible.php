<?php

namespace Drupal\aws_s3_stream_wrapper\StreamWrapper;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;

/**
 * An S3 stream wrapper which is visible in the UI.
 */
class S3StreamWrapperVisible extends S3StreamWrapper {

  /**
   * An S3 stream wrapper which is visible in the Drupal UI.
   *
   * These are stream wrapper bit flags that are the basis for composite types.
   *
   * @var int
   */
  const STREAM_WRAPPER_TYPE = StreamWrapperInterface::READ + StreamWrapperInterface::WRITE + StreamWrapperInterface::VISIBLE;

}
